﻿using UnityEngine;
using System.Collections;
using FyberPlugin;

public class FyberRewardedVideo : MonoBehaviour {

	void OnEnable() {
		// Ad availability
		FyberCallback.AdAvailable += OnAdAvailable;
		FyberCallback.AdNotAvailable += OnAdNotAvailable;   
		FyberCallback.RequestFail += OnRequestFail;
	}

	void OnDisable() {
		// Ad availability
		FyberCallback.AdAvailable -= OnAdAvailable;
		FyberCallback.AdNotAvailable -= OnAdNotAvailable;   
		FyberCallback.RequestFail -= OnRequestFail;
	}

	// [...]

	public Ad rewardedVideoAd;

	public void RequestRewardedVideo() {
		Debug.Log ("[ FyberRewardedVideo ] RequestRewardedVideo");
		RewardedVideoRequester.Create()
		// optional method chaining
		//.AddParameter("key", "value")
		//.AddParameters(dictionary)
		//.WithPlacementId(placementId)
		// changing the GUI notification behaviour when the user finishes viewing the video
		//.NotifyUserOnCompletion(true)
		// you can add a virtual currency requester to a video requester instead of requesting it separately
		//.WithVirtualCurrencyRequester(virtualCurrencyRequester)
		// you don't need to add a callback if you are using delegates
		//.WithCallback(requestCallback)
		// requesting the video
			.Request(); 
	}

	public void ShowVideo() {
		if (rewardedVideoAd != null) {
			rewardedVideoAd.Start();
			rewardedVideoAd = null;
		} else {
			Debug.Log ("[ FyberRewardedVideo ] No_Video");
		}      
	}

	private void OnAdAvailable(Ad ad) {
		switch(ad.AdFormat) {
		case AdFormat.REWARDED_VIDEO:
			rewardedVideoAd = ad;
			Debug.Log ("[ FyberRewardedVideo ] OnAdAvailable");
			break;
			//handle other ad formats if needed
		}
	}
		
	private void OnAdNotAvailable(AdFormat adFormat) {
		switch(adFormat) {
		case AdFormat.REWARDED_VIDEO:
			rewardedVideoAd = null;
			Debug.Log ("[ FyberRewardedVideo ] OnAdNotAvailable");
			break;
			//handle other ad formats if needed
		}
	}

	private void OnRequestFail(RequestError error) {
		// process error
		Debug.Log ("[ FyberRewardedVideo ] error " + error.Description);
	}
}
