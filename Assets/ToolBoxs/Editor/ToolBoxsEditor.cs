﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class ToolBoxsEditor : EditorWindow {

	[MenuItem("GameObject/Add App Log View %j", false, -100)]
	public static void AddAppLogView() {
		GameObject obj = GameObject.Find("AppLogView Canvas");
		if(obj != null) {
			DestroyImmediate (obj);
		}

		GameObject view = Resources.Load<GameObject> ("AppLogViewCanvas");
		obj = Instantiate (view);
		obj.name = "AppLogView Canvas";
		obj.GetComponent<Canvas> ().renderMode = RenderMode.ScreenSpaceCamera;
		obj.GetComponent<Canvas> ().worldCamera = Camera.main;
	}
}
