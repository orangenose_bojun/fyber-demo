﻿using UnityEngine;
using System.Collections;
using FyberPlugin;

public class FyberInterstitial : MonoBehaviour {

	void OnEnable() {
		// Ad availability
		FyberCallback.AdAvailable += OnAdAvailable;
		FyberCallback.AdNotAvailable += OnAdNotAvailable;   
		FyberCallback.RequestFail += OnRequestFail;
		FyberCallback.AdStarted += OnAdStarted;
		FyberCallback.AdFinished += OnAdFinished;  
	}

	void OnDisable() {
		// Ad availability
		FyberCallback.AdAvailable -= OnAdAvailable;
		FyberCallback.AdNotAvailable -= OnAdNotAvailable;   
		FyberCallback.RequestFail -= OnRequestFail;
		FyberCallback.AdStarted -= OnAdStarted;
		FyberCallback.AdFinished -= OnAdFinished;  
	}

	//[...]

	Ad interstitialAd;

	//[...]

	private void OnAdAvailable(Ad ad) {
		switch(ad.AdFormat) {
		case AdFormat.INTERSTITIAL:
			interstitialAd = ad;
			Debug.Log ("[ FyberInterstitial ] OnAdAvailable");
			break;
			//handle other ad formats if needed
		}
	}

	private void OnAdNotAvailable(AdFormat adFormat) {
		switch(adFormat) {
		case AdFormat.INTERSTITIAL:
			interstitialAd = null;
			Debug.Log ("[ FyberInterstitial ] OnAdNotAvailable");
			break;
			//handle other ad formats if needed 
		}
	}

	private void OnRequestFail(RequestError error) {
		// process error
		UnityEngine.Debug.Log("[ FyberInterstitial ] OnRequestFail: " + error.Description);
	}

	private void OnAdStarted(Ad ad) {
		// this is where you mute the sound and toggle buttons if necessary
		switch (ad.AdFormat) {
		case AdFormat.INTERSTITIAL:
			interstitialAd = null;
			Debug.Log ("[ FyberInterstitial ] OnAdStarted");
			break;
			//handle other ad formats if needed 
		}
	}

	private void OnAdFinished(AdResult result) {
		switch (result.AdFormat) {
		case AdFormat.INTERSTITIAL:
			Debug.Log ("[ FyberInterstitial ] OnAdFinished with status " + result.Status + " message:" + result.Message);
			break;
			//handle other ad formats if needed 
		}
	}

	public void RequestInterstitial() {
		Debug.Log ("[ FyberInterstitial ] RequestInterstitial");
		InterstitialRequester.Create()
		// optional method chaining
		//.AddParameter("key", "value")
		//.AddParameters(dictionary)
		//.WithPlacementId(placementId)
		// you don't need to add a callback if you are using delegates
		//.WithCallback(requestCallback)
		// request the ad
			.Request();
	}

	public void ShowInterstitial() {
		if (interstitialAd != null) {
			Debug.Log ("[ FyberInterstitial ] ShowInterstitial");
			interstitialAd.Start();
			interstitialAd = null;
		} else {
			Debug.Log ("[ FyberInterstitial ] No Interstitial");
		}    
	}
}
