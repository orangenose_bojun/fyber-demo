﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;

[InitializeOnLoad]
public class JunEditor : MonoBehaviour {

	[MenuItem ("Jun Editor/Clear Console #c")] // SHIFT + C
	private static void ClearConsole () {
		// This simply does "LogEntries.Clear()" the long way:
		var logEntries = System.Type.GetType("UnityEditorInternal.LogEntries,UnityEditor.dll");
		var clearMethod = logEntries.GetMethod("Clear", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
		clearMethod.Invoke(null,null);
	}
}

#endif
