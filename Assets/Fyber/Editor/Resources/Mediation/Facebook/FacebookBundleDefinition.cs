using UnityEditor;
using UnityEngine;
using System;
using System.IO;


namespace FyberEditor
{
	
	[Serializable]
	[BundleDefinitionAttribute("Facebook", "com.fyber.mediation.facebook.FacebookMediationAdapter", "4.10.0-r4", 5, InternalName="FacebookAudienceNetwork")]
	public class FacebookBundleDefinition : BundleDefinition
	{

		[SerializeField]
		[FyberPropertyAttribute("placementId")]
		private string placementId;

		[SerializeField]
		[FyberPropertyAttribute("bannerPlacementId")]
		private string bannerPlacementId;

		[SerializeField]
		[FyberPropertyAttribute("testDeviceHash")]
		private string testDeviceHash;

		public bool supportBanners()
		{
			return true;
		}
		
	}

	#region Banner temp support
	
	[CustomEditor (typeof (FacebookBundleDefinition),true)]
	public class FacebookBundleDefinitionEditor : BundleDefinitionEditor
	{
		
		readonly GUIContent bannerCompatLibMoreInfo = new GUIContent("Click to get more information");
		Version bannerSupportedVersion = new Version ("8.3.0");
		Version sdkVersion = new Version(FyberPlugin.Fyber.Version);
		bool needsCompatLib = true; 
		bool isCompatLibPresent = File.Exists("Assets/Plugins/Android/fyber-banners-compat.jar");
		
		const string bannerCompatLibPage = "http://developer.fyber.com/content/current/unity/guides/banners-compatibility-guide";

		
		bool isCompatible = false;
		string bVersion = "";
		string bName = "";
		
		public override void OnEnable()
		{
			base.OnEnable();
			var bundle = target as BundleDefinition;
			
			if (bundle != null)
			{
				isCompatible = FyberEditorSettings.IsVersionCompatible(bundle.GetBundleDefinitionAttribute().APIVersion);
				bName = bundle.GetBundleDefinitionAttribute().Name;
				bVersion = bundle.GetBundleDefinitionAttribute().Version;
				
			}
			
			needsCompatLib = sdkVersion.CompareTo(bannerSupportedVersion) < 0 ;
			
			if (isCompatible && ShouldDrawCompatMessage())
			{
				serializedObject.FindProperty("enabled").boolValue = false;
				serializedObject.ApplyModifiedProperties();
			}
			
		}
		
		
		public override void OnInspectorGUI() 
		{
			if (isCompatible && ShouldDrawCompatMessage())
			{
				EditorGUILayout.BeginToggleGroup(bName, false);
				EditorGUILayout.EndToggleGroup();
				
				EditorGUI.indentLevel++;
				
				DrawBannerCompatLibNeeded();
				
				EditorGUILayout.Separator();
				
				EditorGUILayout.LabelField(new GUIContent("Bundle version " + bVersion));
				
				EditorGUI.indentLevel--;
			}
			else
			{
				base.OnInspectorGUI();
			}
			
		}
		
		protected void DrawBannerCompatLibNeeded()
		{
			EditorGUILayout.HelpBox("This bundle requires a compatibility library in order to work with the current plugin version", MessageType.Warning);
			
			if (GUILayout.Button(bannerCompatLibMoreInfo, GUI.skin.label))
				Application.OpenURL(bannerCompatLibPage);
			
		}
		
		private bool ShouldDrawCompatMessage()
		{
			return needsCompatLib && !isCompatLibPresent;
		}
		
	}
	#endregion


}
