using System;
using UnityEditor;
using UnityEngine;

namespace FyberEditor
{
	public class FacebookAssetMenu
	{

#if !UNITY_5
		[MenuItem("Fyber/Mediation Assets Generation/Facebook")]
		public static void GenerateFacebookAsset()
		{
			var asset = ScriptableObject.CreateInstance<FacebookBundleDefinition>();
			AssetDatabase.CreateAsset(asset, "Assets/Fyber/Editor/Resources/Mediation/Facebook/FacebookFyberBundle.asset");
			AssetDatabase.SaveAssets();
		}

		[MenuItem("Fyber/Mediation Assets Generation/Facebook", true)]
		public static bool GenerateFacebookAsset_Validator()
		{
			var asset = Resources.Load<FacebookBundleDefinition>("Mediation/Facebook/FacebookFyberBundle");
			return asset == null;
		}
#endif

	}
}

