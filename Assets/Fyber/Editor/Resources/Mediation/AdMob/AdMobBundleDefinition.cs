using UnityEditor;
using UnityEngine;
using System;
using System.IO;

namespace FyberEditor
{

	[Serializable]
	[BundleDefinitionAttribute("AdMob", "com.fyber.mediation.admob.AdMobMediationAdapter", "9.4.0-r2", 5)]
	public class AdMobBundleDefinition : BundleDefinition
	{

		[SerializeField]
		[FyberPropertyAttribute("ad.unit.id")]
		private string adUnitId;

		[SerializeField]
		[FyberPropertyAttribute("banner.ad.unit.id")]
		private string bannerAdUnitId;

		[SerializeField]
		[FyberPropertyAttribute("addTestDevice")]
		private string[] addTestDevice;

		[SerializeField]
		[FyberPropertyAttribute("isCOPPAcompliant")]
		private bool isCOPPAcompliant;
	}
}
