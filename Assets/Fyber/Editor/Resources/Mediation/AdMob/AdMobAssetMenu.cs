using System;
using UnityEditor;
using UnityEngine;

namespace FyberEditor
{
	public class AdMobAssetMenu
	{

#if !UNITY_5
		[MenuItem("Fyber/Mediation Assets Generation/AdMob")]
		public static void GenerateAdMobAsset()
		{
			var asset = ScriptableObject.CreateInstance<AdMobBundleDefinition>();
			AssetDatabase.CreateAsset(asset, "Assets/Fyber/Editor/Resources/Mediation/AdMob/AdMobFyberBundle.asset");
			AssetDatabase.SaveAssets();
		}

		[MenuItem("Fyber/Mediation Assets Generation/AdMob", true)]
		public static bool GenerateAdMobAsset_Validator()
		{
			var asset = Resources.Load<AdMobBundleDefinition>("Mediation/AdMob/AdMobFyberBundle");
			return asset == null;
		}
#endif

	}
}

