#if UNITY_ANDROID 

using System;

namespace FyberPlugin
{
	public class FacebookNetworkBannerSizes
	{

		private const string NETWORK_NAME = "FacebookAudienceNetwork";

		public static readonly NetworkBannerSize BANNER_50 = new NetworkBannerSize(NETWORK_NAME, new BannerSize(320,50));
		public static readonly NetworkBannerSize BANNER_90 = new NetworkBannerSize(NETWORK_NAME, new BannerSize(320,90));
		public static readonly NetworkBannerSize RECTANGLE_HEIGHT_250 = new NetworkBannerSize(NETWORK_NAME, new BannerSize(300,250));
		
	}
}

#endif // UNITY_ANDROID
