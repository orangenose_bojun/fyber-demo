//
//
// Copyright (c) 2016 Fyber. All rights reserved.
//
//

#import "FYBRewardedVideoController.h"

@class FYBCredentials;

@interface FYBRewardedVideoController (Unity)

@property (nonatomic, strong, readonly) FYBCredentials *credentials;

@end
