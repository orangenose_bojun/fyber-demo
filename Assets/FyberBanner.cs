﻿using UnityEngine;
using System.Collections;
using FyberPlugin;

public class FyberBanner : MonoBehaviour {

	void OnEnable() {
		// Ad availability
		FyberCallback.AdAvailable += OnAdAvailable;
		FyberCallback.AdNotAvailable += OnAdNotAvailable;   
		FyberCallback.RequestFail += OnRequestFail;
	}

	void OnDisable() {
		// Ad availability
		FyberCallback.AdAvailable -= OnAdAvailable;
		FyberCallback.AdNotAvailable -= OnAdNotAvailable;   
		FyberCallback.RequestFail -= OnRequestFail;
	}

	Ad bannerAd;

	private void OnAdAvailable(Ad ad) {
		switch(ad.AdFormat) {
		case AdFormat.BANNER:
			bannerAd = ad;
			Debug.Log ("[ FyberBanner ] OnAdAvailable");
			break;
			//handle other ad formats if needed
		}
	}

	private void OnAdNotAvailable(AdFormat adFormat) {
		switch(adFormat) {
		case AdFormat.BANNER:
			bannerAd = null;
			Debug.Log ("[ FyberBanner ] OnAdNotAvailable");
			break;
			//handle other ad formats if needed
		}
	}

	private void OnRequestFail(RequestError error) {
		// process error
		Debug.Log ("[ FyberBanner ] OnRequestFail " + error.Description);
	}


	public void RequestBanner() {
		BannerRequester.Create()
		// optional method chaining
		//.AddParameter("key", "value")
		//.AddParameters(dictionary)
		//.WithPlacementId(placementId)
		#if UNITY_ANDROID
			.WithNetworkSize(AdMobNetworkBannerSizes.SMART_BANNER)
		#endif
		// you don't need to add a callback if you are using delegates
//			.WithCallback()
		// request the ad
			.Request();
	}

	public void ShowBanner() {
		if (bannerAd != null) {
			bannerAd.Start();
		}
	}

	public void HideBanner() {
		if (bannerAd != null) {
			if(bannerAd.AdFormat == AdFormat.BANNER) {
				((BannerAd)bannerAd).Hide ();
			}
			bannerAd = null;
		}    
	}
}
