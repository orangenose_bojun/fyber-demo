﻿using UnityEngine;
using FyberPlugin;
using UnityEngine.UI;

public class OGKey {
	public static string OGAdUnityAndroidVideoId = "1173956";
}

public class FyberDemo : MonoBehaviour {

	[Header("Script Group")]
	public FyberRewardedVideo FyberRewardedVideo;
	public FyberBanner FyberBanner;
	public FyberInterstitial FyberInterstitial;

	[Header("Button  Group")]
	public Button InitButton;

	public Button RequestRewardedVideoButton;
	public Button ShowVideoButton;

	public Button RequestBannerButton;
	public Button ShowBannerButton;
	public Button HideBannerButton;

	public Button RequestInterstitialButton;
	public Button ShowInterstitialButton;

	void Awake() {
		if(Application.isEditor) {
			InitButton.interactable = false;
		}
		RequestRewardedVideoButton.interactable = false;
		ShowVideoButton.interactable = false;

		RequestBannerButton.interactable = false;
		ShowBannerButton.interactable = false;
		HideBannerButton.interactable = false;

		RequestInterstitialButton.interactable = false;
		ShowInterstitialButton.interactable = false;
	}

	void OnEnable() {
		FyberCallback.NativeError += OnNativeExceptionReceivedFromSDK;
	}

	void OnDisable() {
		FyberCallback.NativeError -= OnNativeExceptionReceivedFromSDK;
	}

	public void _Init () {
		Debug.Log ("[ FyberDemo ] Init");

		string appId = "65732";
		if(Application.platform == RuntimePlatform.Android) {
			appId = "64652";
		} else {
			appId = "65732"; // iOS
		}

		Fyber.With (appId)
		// optional chaining methods
		//.WithUserId(userId)
		//.WithParameters(dictionary)
		//.WithSecurityToken(securityToken)
		//.WithManualPrecaching()
			.Start();

		RequestRewardedVideoButton.interactable = true;
		ShowVideoButton.interactable = true;

		RequestBannerButton.interactable = true;
		ShowBannerButton.interactable = true;
		HideBannerButton.interactable = true;

		RequestInterstitialButton.interactable = true;
		ShowInterstitialButton.interactable = true;
	}


	public void _RequestRewardedVideo() {
		FyberRewardedVideo.RequestRewardedVideo ();
	}

	public void _ShowVideo() {
		FyberRewardedVideo.ShowVideo ();
	}


	public void _RequestBanner() {
		FyberBanner.RequestBanner ();
	}

	public void _ShowBanner() {
		FyberBanner.ShowBanner ();
	}

	public void _HideBanner() {
		FyberBanner.HideBanner ();
	}


	public void _RequestInterstitial() {
		FyberInterstitial.RequestInterstitial ();
	}

	public void _ShowInterstitial() {
		FyberInterstitial.ShowInterstitial ();
	}


	public void OnNativeExceptionReceivedFromSDK(string message) {
		//handle exception
		Debug.Log ("[ FyberDemo ] OnNativeException " + message);
	}
}
